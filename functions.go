package jetfuncs

import (
	"fmt"
	"reflect"
	"strings"

	"github.com/CloudyKit/jet/v3"
)

var Funcs = map[string]interface{}{
	"default": jet.Func(func(a jet.Arguments) reflect.Value {
		a.RequireNumOfArguments("default", 2, 2)
		arg0 := a.Get(0)
		// data 里的值
		ctx := a.Runtime().Context()
		if ctx.Kind() == reflect.Ptr {
			ctx = ctx.Elem()
		}
		// 只判断 map 和 struct
		switch ctx.Kind() {
		case reflect.Map:
			if r := ctx.MapIndex(arg0); r.IsValid() {
				return r
			}
		case reflect.Struct:
			if r := ctx.FieldByName(arg0.String()); r.IsValid() {
				return r
			}
		}
		// 全局变量
		ret := a.Runtime().Resolve(arg0.String())
		if ret.Kind() != reflect.Invalid {
			return ret
		}
		return a.Get(1)
	}),
	"isZero":     isZero,
	"trimPrefix": strings.TrimPrefix,
	"trimSuffix": strings.TrimSuffix,
	"join":       strings.Join,
	"format":     fmt.Sprintf,
}

func isZero(k interface{}) bool {
	if k == nil {
		return true
	}
	switch r := k.(type) {
	case int:
		return k.(int) == 0
	case int8:
		return k.(int8) == 0
	case int16:
		return k.(int16) == 0
	case int32:
		return k.(int32) == 0
	case int64:
		return k.(int64) == 0
	case uint:
		return k.(uint) == 0
	case uint8:
		return k.(uint8) == 0
	case uint16:
		return k.(uint16) == 0
	case uint32:
		return k.(uint32) == 0
	case uint64:
		return k.(uint64) == 0
	case float32:
		return k.(float32) == 0
	case float64:
		return k.(float64) == 0
	case bool:
		return !k.(bool)
	case string:
		return k.(string) == ""
	case reflect.Value:
		return _isZero(r)
	case interface{ IsZero() bool }:
		return r.IsZero()
	}
	return _isZero(reflect.ValueOf(k))
}

func _isZero(val reflect.Value) bool {
	if !val.IsValid() {
		return true
	}
	switch val.Kind() {
	case reflect.Array:
		for i := 0; i < val.Len(); i++ {
			if !_isZero(val.Index(i)) {
				return false
			}
		}
		return true
	case reflect.Map, reflect.Slice, reflect.String:
		return val.Len() == 0
	case reflect.Bool:
		return !val.Bool()
	case reflect.Complex64, reflect.Complex128:
		return val.Complex() == 0
	case reflect.Chan, reflect.Func, reflect.Interface, reflect.Ptr:
		return val.IsNil()
	case reflect.Int, reflect.Int8, reflect.Int16, reflect.Int32, reflect.Int64:
		return val.Int() == 0
	case reflect.Float32, reflect.Float64:
		return val.Float() == 0
	case reflect.Uint, reflect.Uint8, reflect.Uint16, reflect.Uint32, reflect.Uint64, reflect.Uintptr:
		return val.Uint() == 0
	case reflect.Struct:
		for i := 0; i < val.NumField(); i++ {
			if !_isZero(val.Field(i)) {
				return false
			}
		}
		return true
	}
	panic("unknown type in isZero " + val.Type().String())
}
